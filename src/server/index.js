const express = require('express')

const PORT = 8080

const app = express()

app.use('/scripts', express.static('./build'))
app.set('view engine', 'vash')
app.set('views', './src/server')
app.get('/', (req, res) => {
  res.render('playerTemplate', { scriptUrl: `http://localhost:${PORT}/scripts/playerExample.js` })
})

console.log(`http://localhost:${PORT}`)

app.listen(PORT)
