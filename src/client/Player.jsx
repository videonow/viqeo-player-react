import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class ViqeoPlayer extends Component {
  constructor(props) {
    super(props)
    this._playerRef = null
    this._player = null
    this._setPlayerRef = (element) => {
      this._playerRef = element
    }
  }

  componentWillUnmount() {
    if (this._player) {
      this._player.destroy()
    }
  }

  componentDidMount() {
    const SCRIPT_URL = __CONFIG__.starterUrl
    if (!document.querySelector(`script[src="${SCRIPT_URL}"]`)) {
      const script = document.createElement('script')
      script.src = SCRIPT_URL
      document.head.appendChild(script)
    }
    const {
      videoId, videoSrc, playerId, previewSrc, profileId,
    } = this.props
    const start = (VIQEO) => {
      if (!this._playerRef) {
        return
      }
      VIQEO.createPlayer({
        videoId, videoSrc, playerId, previewSrc, profileId, parent: this._playerRef,
      }).then((player) => {
        if (this._playerRef) {
          this._player = player
        } else {
          player.destroy()
        }
      })
    }
    if (window.VIQEO) {
      start(window.VIQEO)
    } else if (typeof window.onViqeoLoad === 'undefined') {
      window.onViqeoLoad = [start]
    } else if (typeof window.onViqeoLoad === 'function') {
      window.onViqeoLoad = [window.onViqeoLoad, start]
    } else {
      window.onViqeoLoad.push(start)
    }
  }

  render() {
    return <div ref={this._setPlayerRef} style={{ width: '100%', height: '100%' }}/>
  }
}

ViqeoPlayer.propTypes = {
  videoId: PropTypes.string,
  playerId: PropTypes.number,
  videoSrc: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({ url: PropTypes.string })),
    PropTypes.string,
  ]),
  previewSrc: PropTypes.string,
  profileId: PropTypes.number,
}
