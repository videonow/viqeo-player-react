export default {
  STANDARD: 'standard',
  EXTERNAL: 'external',
  EXTERNAL_HLS: 'external_hls',
  EXTERNAL_MULTI_SRC: 'external_multi_src',
  EXTERNAL_YOUTUBE_SRC: 'external_youtube_src',
  RECOMMENDATION: 'recommendation',
}
