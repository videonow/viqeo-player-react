import React from 'react'
import ReactDOM from 'react-dom'
import playersEnums from './playersEnums'
import ViqeoPlayer from './Player'

const QUERY_PARAM = 'playerType'

const urlParams = new URLSearchParams(window.location.search)

const playerType = urlParams.get(QUERY_PARAM)

let component = <div>
  {playerType ? <div>Not exist: {playerType}</div> : null}
  {Object.keys(playersEnums).map((key) => (
    <div key={key}><a href={`/?${QUERY_PARAM}=${playersEnums[key]}`}>{key}</a></div>
  ))}
</div>


if (playerType === playersEnums.STANDARD) {
  component = <ViqeoPlayer videoId={'c11361729489335aa420'} profileId={430}/>
} else if (playerType === playersEnums.RECOMMENDATION) {
  component = <ViqeoPlayer playerId={332} profileId={1142} />
} else if (playerType === playersEnums.EXTERNAL) {
  component = <ViqeoPlayer
    playerId={78}
    profileId={431}
    videoSrc={'https://cdn.viqeo.tv/storage/bf/d0/9fa9bf094c1f0f2db4ec565814377bac.mp4'}
    previewSrc={'https://cdn.viqeo.tv/storage/bf/d0/188d04386895840ff8044ace257b97ee.jpg'}
  />
} else if (playerType === playersEnums.EXTERNAL_YOUTUBE_SRC) {
  component = <ViqeoPlayer
    playerId={78}
    profileId={431}
    videoSrc={'https://www.youtube.com/watch?v=COwlqqErDbY&_=0'}
  />
} else if (playerType === playersEnums.EXTERNAL_HLS) {
  component = <ViqeoPlayer
    playerId={78}
    profileId={431}
    videoSrc={'https://cdn.viqeo.tv/storage/hls/,bf/d0/9fa9bf094c1f0f2db4ec565814377bac.mp4,bf/d0/8d366f3e066b6120de15af31285f7faf.mp4,.urlset/master.m3u8'}
  />
} else if (playerType === playersEnums.EXTERNAL_MULTI_SRC) {
  component = <ViqeoPlayer
    playerId={78}
    profileId={431}
    videoSrc={[{
      url: 'https://cdn.viqeo.tv/storage/bf/d0/9fa9bf094c1f0f2db4ec565814377bac.mp4',
    }, {
      url: 'https://cdn.viqeo.tv/storage/bf/d0/8d366f3e066b6120de15af31285f7faf.mp4',
    }]}
  />
}

ReactDOM.render(component, document.getElementById('player'))
