# Installation

```
npm i viqeo-player-react --save
```

# Usage

### Import
```javascript
import ViqeoPlayer from 'viqeo-player-react'
```

### Standard player
```javascript
<ViqeoPlayer videoId={'c11361729489335aa420'} profileId={430}/>
```

### Reccommendation player

```javascript
<ViqeoPlayer playerId={332} profileId={1142} />
```

### External player (mp4)

```javascript
<ViqeoPlayer
  playerId={78}
  profileId={431}
  videoSrc={'https://cdn.viqeo.tv/storage/bf/d0/9fa9bf094c1f0f2db4ec565814377bac.mp4'}
  previewSrc={'https://cdn.viqeo.tv/storage/bf/d0/188d04386895840ff8044ace257b97ee.jpg'} />
```

### External player (youtube link)

```javascript
<ViqeoPlayer
    playerId={78}
    profileId={431}
    videoSrc={'https://www.youtube.com/watch?v=COwlqqErDbY&_=0'}
  />
```

### External player (hls)

```javascript
<ViqeoPlayer
    playerId={78}
    profileId={431}
    videoSrc={'https://cdn.viqeo.tv/storage/hls/,bf/d0/9fa9bf094c1f0f2db4ec565814377bac.mp4,bf/d0/8d366f3e066b6120de15af31285f7faf.mp4,.urlset/master.m3u8'}
  />
```

### External player (mp4 links)

```javascript
<ViqeoPlayer
    playerId={78}
    profileId={431}
    videoSrc={[{
      url: 'https://cdn.viqeo.tv/storage/bf/d0/9fa9bf094c1f0f2db4ec565814377bac.mp4',
    }, {
      url: 'https://cdn.viqeo.tv/storage/bf/d0/8d366f3e066b6120de15af31285f7faf.mp4',
    }]}
  />
```
