const path = require('path')
const webpack = require('webpack')

module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  entry: {
    playerExample: './src/client/playerExample.jsx',
  },
  output: {
    path: path.resolve('build'),
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new webpack.DefinePlugin({
      __CONFIG__: JSON.stringify({ starterUrl: 'https://cdn.viqeo.tv/js/vq_starter.js' }),
    }),
  ],
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: [/node_modules/],
      loader: [{ loader: 'babel-loader' }],
    }],
  },
}
