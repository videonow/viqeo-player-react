const path = require('path')
const webpack = require('webpack')

module.exports = {
  mode: 'production',
  entry: {
    Player: './src/client/Player.jsx',
  },
  output: {
    path: path.resolve('build'),
    filename: 'Player.js',
    library: 'video-player-react',
    libraryTarget: 'umd',
    // https://github.com/webpack/webpack/issues/6522
    globalObject: 'typeof self !== \'undefined\' ? self : this',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new webpack.DefinePlugin({
      __CONFIG__: JSON.stringify({ starterUrl: 'https://cdn.viqeo.tv/js/vq_starter.js' }),
    }),
  ],
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: [/node_modules/],
      loader: [{ loader: 'babel-loader' }],
    }],
  },
  externals: {
    react: {
      root: 'React',
      commonjs2: 'react',
      commonjs: 'react',
      amd: 'react',
      umd: 'react',
    },
    'react-dom': {
      root: 'ReactDOM',
      commonjs2: 'react-dom',
      commonjs: 'react-dom',
      amd: 'react-dom',
      umd: 'react-dom',
    },
  },
}
